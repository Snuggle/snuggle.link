# https://snuggle.link
## Summary

| Status        | Count |
|---------------|-------|
| 🔍 Total      | 46    |
| ✅ Successful | 43    |
| ⏳ Timeouts   | 0     |
| 🔀 Redirected | 0     |
| 👻 Excluded   | 3     |
| ❓ Unknown    | 0     |
| 🚫 Errors     | 0     |


_(Automatically updating link status)_


---

My own lil URL shortener, forked from [cassidoo/cass.run](https://github.com/cassidoo/cass.run)

[![Netlify Status](https://api.netlify.com/api/v1/badges/e5ab1a8f-e8d1-498b-b507-f1a00ebb145b/deploy-status)](https://app.netlify.com/sites/snuggle-link/deploys)

[![Links](https://github.com/Snuggle/snuggle.link/actions/workflows/links.yml/badge.svg)](https://github.com/Snuggle/snuggle.link/actions/workflows/links.yml) [![Mirroring](https://github.com/Snuggle/snuggle.link/actions/workflows/main.yml/badge.svg)](https://github.com/Snuggle/snuggle.link/actions/workflows/main.yml) 
## Make your own

Clicking this button will clone the repo and deploy it to Netlify, be sure to rename your repo afterwards!

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/cassidoo/cass.run&utm_source=github&utm_medium=shortener-cs&utm_campaign=devex)

You can add a link by running `npm run shorten websitelink path` where `websitelink` is the route you want to shorten, and `path` is the path that'll replace it. If you omit `path`, it'll give you a random string!

# List of Shortened Links
The [list of shortened links is located in `_redirects`](https://github.com/Snuggle/snuggle.link/blob/main/_redirects).
